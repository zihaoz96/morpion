package Controller;

import java.awt.Point;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import modal.ModalDialog;
import view.Launcher;

public class GamePageController implements Initializable{
	
	// true vsPlayer; false vsPc
	public static boolean MODE = true;
	
	// difficult the game
	public static int LEVEL = 0;
	
	// state cheese board
	private String state;
	
	// for draw game board
	private GraphicsContext gc;
	
	/*
	 * round = true   =>   round 'X'  bamboo
	 * round = false   =>   round 'O'  panda
	 */
	private boolean round = true;
	
	// draw line with point the start and the end
	private Point start = new Point();
	private Point end = new Point();
	
	// AnchorPane for get stage
	@FXML
	private AnchorPane root;
	
	// canvas game board
	@FXML
	private Canvas canvas;
	
	// show score player1, player2 and notification
	@FXML
	private Text scoreX,scoreO,notif;
	
	// replay the game or back to page initial
	@FXML
	private Button replay,back;
	
	// menu bar for mode, preference and help
	@FXML
	private MenuBar menuBar;
	
	// imageView for 9 cells
	@FXML
	private ImageView c0,c1,c2,c3,c4,c5,c6,c7,c8,scoreBamboo,scorePanda,p1,p2;
	
	// data AI
	private ReadData dataAI = new ReadData();
	
	// list image
	ArrayList<ImageView> listImage = new ArrayList<ImageView>();
	
	// set image from local
	Image panda = new Image(getClass().getResourceAsStream("/img/panda.png"));
	Image bamboo = new Image(getClass().getResourceAsStream("/img/bamboo.png"));
	Image gamer = new Image(getClass().getResourceAsStream("/img/gamer.png"));
	Image robot = new Image(getClass().getResourceAsStream("/img/robot.png"));
	
	// create fade transition
	FadeTransition fadeTransition;
	
	// color white
    Color whiteColor = Color.rgb(255, 255, 255);
    // color red
    Color redColor = Color.rgb(235, 50, 25 );
	
	/*
	 * initialize
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		initializeState();
		
		// before 1 second
		new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
            	// draw map
        		drawMap();
        		// initial score
        		rescore();
            }
        }, new Date(System.currentTimeMillis() + 1000));
		
		root.getChildren().add(canvas);
		
		mouseEvent(replay);
		mouseEvent(back);
		
		// create and add menus
		Menu mode = new Menu();
		Menu parm = new Menu();
		Menu aide = new Menu();
		Label labelMode = new Label("Mode");
		Label labelParm = new Label("Param��tres");
		Label labelAide = new Label("Aide");
		
		// add listener for each menuBar
		labelMode.setOnMouseClicked(event -> {
			try {
				mode();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		labelParm.setOnMouseClicked(event -> {
			try {
				preference();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		labelAide.setOnMouseClicked(event -> {
			help();
		});
		
		// set label for each menu
		mode.setGraphic(labelMode);
		parm.setGraphic(labelParm);
		aide.setGraphic(labelAide);
		
		// add into menuBar
		menuBar.getMenus().addAll(mode,parm,aide);
		
		// load image
		scoreBamboo.setImage(bamboo);
		scorePanda.setImage(panda);
		
		// default player 1 and player 2  
		p1.setImage(gamer);
		p2.setImage(gamer);
		
		// add imageView into list
		listImage.add(c0);
		listImage.add(c1);
		listImage.add(c2);
		listImage.add(c3);
		listImage.add(c4);
		listImage.add(c5);
		listImage.add(c6);
		listImage.add(c7);
		listImage.add(c8);
	}
	
	// initial the score in zero
	private void rescore() {
		scoreX.setText("-");
		scoreO.setText("-");
	}
	
	// show underline and cursor hand
	private void mouseEvent(Button btn) {
		btn.setOnMouseEntered(event -> {
			btn.setUnderline(true);
			btn.setCursor(Cursor.HAND);
		});
		
		btn.setOnMouseExited(event ->{
			btn.setUnderline(false);
			btn.setCursor(Cursor.DEFAULT);
		});
	}
	
	// initial game board and their state
	private void initializeState() {
		// state initial
		this.state = "000000000";
		// GraphicsContext 
		gc = canvas.getGraphicsContext2D();
		
		// click listener
		canvas.setOnMouseClicked(event -> {
			// get location
	        double x = event.getX(), y = event.getY();
	        canvasClick(x, y);
	    });
	}
	
	// draw game board
	private void drawMap() {
		
		Path path = createPath(0, 170, 520, 170);
		
		Animation animation = createPathAnimation(path, Duration.seconds(1), whiteColor);
		
        animation.play();
        
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                animation.stop();
            }
        }, new Date(System.currentTimeMillis() + 1000));
        
	}
	
	// location x1,y1 to x2,y2
	private Path createPath(int x1, int y1, int x2, int y2) {
		// create line path
		Path path = new Path();
		
		// draw line
		path.getElements().add(new MoveTo(x1, y1));
		path.getElements().add(new LineTo(x2, y2));
		
		return path;
	}
	
	private Animation createPathAnimation(Path path, Duration duration, Color lineColor) {
		// move a node along a path. we want its position
        Circle pen = new Circle(0, 0, 4);
       
        
        // create path transition
        PathTransition pathTransition = new PathTransition( duration, path, pen);
        pathTransition.currentTimeProperty().addListener( new ChangeListener<Duration>() {
        	
        	Location oldLocation = null;

			@Override
			public void changed(ObservableValue<? extends Duration> arg0, Duration arg1, Duration arg2) {
				// skip starting at 0/0
                if( arg1 == Duration.ZERO)
                    return;
                
                // get current location
                double x = pen.getTranslateX();
                double y = pen.getTranslateY();
				
                // initialize the location
                if( oldLocation == null) {
                    oldLocation = new Location();
                    oldLocation.x = x;
                    oldLocation.y = y;
                    return;
                }
                
                // set line color
                gc.setStroke(lineColor);
                // set line width
                gc.setLineWidth(20);
                // draw line
                gc.strokeLine(oldLocation.x, oldLocation.y, x, y);
                gc.strokeLine(oldLocation.y, oldLocation.x, y, x);
                gc.strokeLine(oldLocation.x, oldLocation.y+180, x, y+180);
                gc.strokeLine(oldLocation.y+180, oldLocation.x, y+180, x);

                // update old location with current one
                oldLocation.x = x;
                oldLocation.y = y;
			}

        });
        
        return pathTransition;
	}
	
	// set location
	public static class Location {
        double x;
        double y;
    }
	
	// return feedback when player click
	private void feedback(int index) {
		/*
		 * 1 -> player 1
		 * 2 -> player 2
		 */
		char outPrint = '0';
		
		// index 0-8, get cell that player clicked
		ImageView img = listImage.get(index);
		
		// if this cell empty
		if (state.charAt(index) == '0') {
			// if it's round player 1
			if (round) {
				outPrint = '1';
				img.setImage(bamboo);
				notif.setText("C'est a panda de joeur");
			} 
			// round player 2
			else {
				outPrint = '2';
				img.setImage(panda);
				notif.setText("C'est a bambou de joeur");
			}
			
			// update string state
			StringBuilder strBuilder = new StringBuilder(state);
			strBuilder.setCharAt(index, outPrint);
			state = strBuilder.toString();
			
			// exchange
			round = !round;
			
			// check win already
			if(isWin(outPrint)) {
				// draw line that we have 3
				gc.setLineWidth(10);
				draw_winLine();
				gc.stroke();
				
				// notification
				notif.setText("Partie terminee");
				
				// score of winner +1
				changeScore(outPrint);
				
				// close mouse click
				canvas.setOnMouseClicked(null);
				
				return;
			}
		}
	}
	
	// function for change score
	private void changeScore(char winner) {
		if (winner == '1') {
			if (scoreX.getText() == "-") {
				scoreX.setText("1");
			} else {
				scoreX.setText(String.valueOf((Integer.valueOf(scoreX.getText())+1)));
			}
		}else {
			if (scoreO.getText() == "-") {
				scoreO.setText("1");
			} else {
				scoreO.setText(String.valueOf((Integer.valueOf(scoreO.getText())+1)));
			}
		}
	}
	
	// verify the cell that player click and send data to function feedback
	public void canvasClick(Double x, Double y) {
		
		if (x<160) {
			// cell 0
			if (y<160) {
				feedback(0);
			}
			// cell 3
			else if (y>180 && y<340) {
				feedback(3);
			}
			// cell  6
			else if (y>360){
				feedback(6);
			}
		}else if (x>180 & x<340) {
			// cell 1
			if (y<160) {
				feedback(1);
			}
			// cell 4
			else if (y>180 && y<340) {
				feedback(4);
			}
			// cell 7
			else if (y>360){
				feedback(7);
			}
		}else if (x>360) {
			// cell 2
			if (y<160) {
				feedback(2);
			}
			// cell 5
			else if (y>180 && y<340) {
				feedback(5);
			}
			// cell 8
			else if (y>360){
				feedback(8);
			}
		}
		
		if (isWin('1')) {
			return;
		}
		
		// if no more cell empty
		if (checkCell(state).isEmpty() ) {
			// if nobody win
			if (!notif.getText().equals("Partie termin��e")) {
				notif.setText("Draw ! ");
			}
			return;
		}
		
		// if mode with AI, and round AI
		if (MODE==false && round==false) {
			// level 0 random choice
			if (LEVEL == 0) {
				actionRandom();
			} 
			// else chose action strategy
			else {
				actionStrategie();
			}
		}
	}
	
	// random action for AI
	private void actionRandom() {
		ArrayList<Integer> arrayList = checkCell(state);
		int action = arrayList.get(new Random().nextInt(arrayList.size()));
		feedback(action);
	}
	
	// return action strategy
	private void actionStrategie() {
		// all of cell is possible to chose 
		ArrayList<Integer> actionPossible = checkCell(state);
		
		// check state in training record
		if (dataAI.states.containsKey(state)) {
			// all of action possibility of winning
			Double[] actions = dataAI.states.get(state);
			
			// initial action is the first of list
			double bestAction = actions[actionPossible.get(0)];
			int action = actionPossible.get(0);
			for (int i = 0; i < actionPossible.size(); i++) {
				
				// possibility between 0 and 1, so we chose the biggest
				if (actions[actionPossible.get(i)] > bestAction) {
					bestAction = actions[actionPossible.get(i)];
					action = actionPossible.get(i);
				}
			}
			// send action to feed back
			feedback(action);
		}
		// state not in record
		else {
			actionRandom();
		}
		
	}
	
	// return empty cell
	public ArrayList<Integer> checkCell(String state) {
		ArrayList<Integer> resArrayList = new ArrayList<Integer>();
		for (int i = 0; i < state.length(); i++) {
			if (state.charAt(i) == '0') {
				resArrayList.add(i);
			}
		}
		
		return resArrayList;
	}
	
	// check is win or not, 9 condition to win
	private boolean isWin(char c) {

		// if is win already, return true and save coordinate the point start and the point end
        if (state.charAt(0) == c) {
			if (state.charAt(1) == c && state.charAt(2) == c) {start.setLocation(30,80); end.setLocation(490,80);return true;}
			else if (state.charAt(4) == c && state.charAt(8) == c) {start.setLocation(30,30); end.setLocation(490,490);return true;}
			else if (state.charAt(3) == c && state.charAt(6) == c) {start.setLocation(80,30); end.setLocation(80,490);return true;}
		} 
        if (state.charAt(2) == c) {
			if (state.charAt(5) == c && state.charAt(8) == c) {start.setLocation(440,30); end.setLocation(440,490);return true;}
			else if (state.charAt(4) == c && state.charAt(6) == c) {start.setLocation(490,30); end.setLocation(30,490);return true;}
		} 
		if (state.charAt(1) == c && state.charAt(4) == c && state.charAt(7) == c) {start.setLocation(260,30); end.setLocation(260,490);return true;}
		else if (state.charAt(3) == c && state.charAt(4) == c && state.charAt(5) == c) {start.setLocation(30,250); end.setLocation(490,250);return true;}
		else if (state.charAt(6) == c && state.charAt(7) == c && state.charAt(8) == c) {start.setLocation(30,440); end.setLocation(480,440);return true;}
			
		return false;
	}
	
	// condition win, draw line
	private void draw_winLine() {
		gc.setStroke(Color.rgb(235, 50, 25 ));
		gc.strokeLine(start.getX(), start.getY(), end.getX(), end.getY());
	}
	
	@FXML
	public void replay(ActionEvent event) {
		replay();
	}
	
	// switch to initial page
	@FXML
	public void backToMenu(ActionEvent event) {
		rescore();
		replay(event);
		this.switchTo(Launcher.initial);
	}
	
	// replay the game
	public void replay() {
		// clear UI
		gc.clearRect(0, 0, 520, 520);
		
		// initialize stage
		initializeState();
		
		// initialize image null for each cell
		initializeImageView();
		
		// initialize map
		drawMap();
		
		// round of panda
		round = true;
		
		// set text
		notif.setText("Demarrez le jeu.");
	}
	
	public void fadeTransition(int second) {
		// create fade transition duration n seconds
		fadeTransition = new FadeTransition(Duration.seconds(second), this.root);
		// starting transparency is 1
		fadeTransition.setFromValue(1);
		// end transparency is 0.1
		fadeTransition.setToValue(0.1);
		// set cycle indefinite
		fadeTransition.setCycleCount(Timeline.INDEFINITE);
		// reverse true
		fadeTransition.setAutoReverse(true);
		// start transition
		fadeTransition.play();
	}
	
	// if we change mode (VS human or VS AI)
	public void mode() throws IOException {
		// load param.fxml
		Parent paramLayout = FXMLLoader.load(getClass().getResource("/view/Param.fxml"));
		paramLayout.setStyle("-fx-background-image: url('" + "/img/background-image.png" + "')");
		Scene scene = new Scene(paramLayout, 600, 500);
		
		// create modalDialog and set scene Param
		ModalDialog modal = new ModalDialog(this.root.getScene(),scene);
		modal.modalMode();
		Stage modalStage = (Stage) scene.getWindow();
		
		// fade transition 4 seconds
		fadeTransition(4);
		
		// set listener when we hind or close modal 
		closeHandler(modalStage);
	}
	
	// click preference
	public void preference() throws IOException {
		// load SettingAI.fxml
		Parent paramLayout = FXMLLoader.load(getClass().getResource("/view/SettingAI.fxml"));
		paramLayout.setStyle("-fx-background-image: url('" + "/img/background-image.png" + "')");
		Scene scene = new Scene(paramLayout, 600, 500);
		
		// fade transition 4 seconds
		fadeTransition(4);
		
		// create modal
		ModalDialog modal = new ModalDialog(this.root.getScene(),scene);
		modal.modalPreference();
		Stage modalStage = (Stage) scene.getWindow();
		
		// set listener when we hind or close modal 
		closeHandler(modalStage);
	}
	
	private void closeHandler(Stage modalStage) {
		// set listener when we hind modal 
		modalStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				// stop the transition
				fadeTransition.jumpTo(Duration.ZERO);
				fadeTransition.stop();
			}
			
		});
		
		// set listener when we hind modal 
		modalStage.setOnHidden(new EventHandler<WindowEvent>() {
			
			@Override
			public void handle(WindowEvent event) {
				// restart the game
				replay();
				rescore();
				
				// stop the transition
				fadeTransition.jumpTo(Duration.ZERO);
				fadeTransition.stop();
				
				// mode play with AI
				if (MODE == false) {
					startModeAI();
				} else {
					p2.setImage(gamer);
				}
			}
		});
	}
	
	// start mode AI
	public void startModeAI() {
		// player 2 robot change image
		p2.setImage(robot);
		// level easy, AI random action
		if (LEVEL == 1) {
			try {
				FileInputStream inputStream = new FileInputStream("./src/levelMoyen.txt");
				dataAI.readFromFile(inputStream);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		// level 2 no defense just attack
		else if (LEVEL == 2) {
			try {
				FileInputStream inputStream = new FileInputStream("./src/levelDiff.txt");
				dataAI.readFromFile(inputStream);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		// level difficult attack and defense
		else if (LEVEL == 3) {
			try {
				FileInputStream inputStream = new FileInputStream("./src/levelDiy.txt");
				dataAI.readFromFile(inputStream);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
	
	// create modal help
	public void help() {
		ModalDialog modal = new ModalDialog();
		modal.modalHelp();
	}
	
	// set all cell null
	private void initializeImageView() {
		c0.setImage(null);
		c1.setImage(null);
		c2.setImage(null);
		c3.setImage(null);
		c4.setImage(null);
		c5.setImage(null);
		c6.setImage(null);
		c7.setImage(null);
		c8.setImage(null);
	}
	
	// switch to other scene
	private void switchTo(Scene scene) {
		Stage primaryStage = (Stage) this.root.getScene().getWindow();
		
		primaryStage.setScene(scene);
		
		primaryStage.show();
	}
}
