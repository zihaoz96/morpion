package qLearning;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/*
 * Difficult moyen version 1, just attack no defense 
 */
public class DifficulteMoyen {
	// learning rate
	private double alpha = 0.01;

	// determines the importance of future rewards
	private double gamma = 0.9;
	
	// possibility random action 
	private double greedy = 0.90;
	
	// reward win and loss
	double rewardWin = 1.0;
	double rewardLoss = -0.5;
	
	// state and actions, which action is possibility of winning 
	private Map<String, Double[]> states = new HashMap<String, Double[]>();
	
	// epreuve
	private int testRound = 2000000; 
	
	// game round
	private int round = 0;
	
	// return cell empty
	public ArrayList<Integer> checkCell(String state) {
		ArrayList<Integer> resArrayList = new ArrayList<Integer>();
		for (int i = 0; i < state.length(); i++) {
			if (state.charAt(i) == '0') {
				resArrayList.add(i);
			}
		}
		return resArrayList;
	}
	
	// human choose 
	public int humanChoose(String state) {
		ArrayList<Integer> cellFree = checkCell(state);
		int action = cellFree.get(new Random().nextInt(cellFree.size()));
	
		return action;
	}
	
	// AI choose
	public int chooseAction(String state) {
		// get all actions in this state
		Double[] state_actions = states.get(state);
		
		// get these actions empty
		ArrayList<Integer> cellFree = checkCell(state);
		
		// initial the first action we choose
		int action = 0;
		
		// random if first step or float > greedy 
		if (Math.random() > greedy || allZero(state_actions)) {
			action = cellFree.get(new Random().nextInt(cellFree.size()));
		} 
		
		// we choose index of the biggest in action
		else {
			double max = state_actions[cellFree.get(0)];
			action = cellFree.get(0);
			for (int i = 0; i < cellFree.size(); i++) {
				if (state_actions[cellFree.get(i)] > max) {
					max = state_actions[cellFree.get(i)];
					action = cellFree.get(i);
				}
			}
		}
		
		return action;
	}
	
	/*
	 * true : no suggestion
	 * false : there are some record good or bad
	 */
	public boolean allZero(Double[] actions) {
		for (int i = 0; i < actions.length; i++) {
			if (actions[i] != 0.0) {
				return false;
			}
		}
		return true;
	}
	
	// return reward and next state
	public String[] feedback(String state, int action, Character player) {
		// calculate next state
		String nextState = nextState(state, action, player);
		
		// get reward
		double reward = getReward(nextState);
		
		// if someone have already win, nextState = done
		if (reward == rewardLoss) {
			nextState = "done";
		} else if (reward == rewardWin) {
			nextState = "done";
		} 
		
		// turn back two value
		String[] res = {nextState,""+reward};
		return res;
	}
	
	// calculate next state
	public String nextState(String state, int action, Character player) {
		String nextState = "";
		StringBuilder strBuilder = new StringBuilder(state);
		
		// set action in state
		strBuilder.setCharAt(action, player);
		
		// transform type string
		nextState = strBuilder.toString();
		
		// set into HashMap
		checkState(nextState);
		
		return nextState;
	}
	
	// calculate reward
	public double getReward(String state){
		int[][] win = {{0,1,2},{0,3,6},{0,4,8},{2,5,8},{2,4,6},{1,4,7},{3,4,5},{6,7,8}};
		for (int i = 0; i < win.length; i++) {
			if (state.charAt(win[i][0]) != '0') {
				if (state.charAt(win[i][0]) == state.charAt(win[i][1]) && state.charAt(win[i][0]) == state.charAt(win[i][2])) {
					if (state.charAt(win[i][0]) == '1') {
						// AI1 win reward
						return rewardLoss;
					} else {
						// AI2 win reward 
						return rewardWin;
					}
				}
			}
		}
		return 0;
	}
	
	// turn back max value in this state (possibility)
	public double getMaxValue(String state) {
		Double[] tmp = states.get(state);
		double max = tmp[0];
		for (int i = 0; i < tmp.length; i++) {
			if (tmp[i]>max) {
				max = tmp[i];
			}
		}		
		return max;
	}
	
	// learning algorithm
	public void learn(String state, String nextState, double reward, int action) {
		double q_predict = states.get(state)[action];
		double q_traget = 0;
		
		// check final
		if (nextState == "done") {
			q_traget = reward;
		} else {
			q_traget = reward + gamma * getMaxValue(nextState);
		}
		states.get(state)[action] = (1-alpha) * states.get(state)[action] + alpha * (q_traget - q_predict);
	}
	
	// if not in HashMap, add
	public void checkState(String state) {
		if (!this.states.containsKey(state)) {
			Double[] tmp = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
			this.states.put(state, tmp);
		}
	}
	
	// tester
	public void test() {
		// score initial
		int AIWin = 0;
		int HumanWin = 0;
		int draw = 0;
		
		// chance to win
		double chance = 0.0;
		
		// test 
		for (int i = 0; i < testRound; i++) {
			// initial state
			String state = "000000000";
			checkState(state);
			// is terminated no
			boolean isTerminated = false;
			
			// circulate
			while (!isTerminated) {
				// initial reward 0 
				double reward = 0;
				
				// initial nextState empty
				String nextState = "";
				
				// player1 choose
				int actionHuman = humanChoose(state);
				String[] resHuman = feedback(state, actionHuman, '1');
				String stateHuman = resHuman[0];
				reward = Double.valueOf(resHuman[1]);

				learn(state, stateHuman, reward, actionHuman);
				// player2 win
				if (reward == rewardLoss) {
					HumanWin++;
					break;
				} else if (getReward(stateHuman) == 0 && checkCell(stateHuman).size() == 0) {
					draw ++;
					break;
				} 
				
				int actionAI = chooseAction(stateHuman);
				String[] resAI = feedback(stateHuman, actionAI, '2');
				nextState = resAI[0];
				reward = Double.valueOf(resAI[1]);
				
				learn(stateHuman, nextState, reward, actionAI);
				
				if (reward == rewardWin) {
					AIWin++;
					break;
				}
				
				state = nextState;
			}
			round++;
			if (round%50==0) {
				chance = (double) AIWin/round;
				System.out.println("AI win: "+chance+"   round"+round);
			}
		}
		
		try {
			File file = new File("./resultMoyen.txt");
			FileWriter out = new FileWriter(file);
			for (Map.Entry<String, Double[]> entry : states.entrySet()) {
				 
			    String state = entry.getKey();
			    Double[] actions = entry.getValue();
			    out.write(state+"\t");
			    for (int j = 0; j < actions.length; j++) {
			    	out.write(actions[j]+"\t");
				}
			    out.write("\r\n");
			}
			out.write(states.size());
			out.close();
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		if (true) {
			double a =(double) HumanWin/round;
			double b =(double) AIWin/round;
			double c =(double) draw/round;
			System.out.println("HumanWin: "+  a + " AIwin:" +b + " draw: " + c);
		}
		
	}
	
	public static void main(String[] args) throws IOException {
		DifficulteMoyen qTable = new DifficulteMoyen();
		qTable.test();
	}
}
