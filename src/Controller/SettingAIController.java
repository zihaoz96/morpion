package Controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import qLearning.Q_Learning;

public class SettingAIController implements Initializable{

	@FXML
	private AnchorPane root;
	
	@FXML
	private TextField alpha, gamma, epreuve;
	
	@FXML
	private TextArea textArea;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		checkInput(alpha);
		checkInput(gamma);
		
		// listener when player taper number
		epreuve.focusedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (!newValue) {
					String input = epreuve.getText();
					// limit training time 100 - 99999999
					String reg = "^\\d{3,8}$";
					if (!input.matches(reg)) {
						epreuve.setText("");
					}
				}
			}
		});
		
	}

	// check text input 0-1
	public void checkInput(TextField textField) {
		textField.focusedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (!newValue) {
					// get text input
					String input = textField.getText();
					String reg = "^[0-9]+(.[0-9]+)?$";
					// check Regulier
					if (!input.matches(reg)) {
						textField.setText("");
					} else {
						double value = Double.parseDouble(input);
						if (value<=0 || value>1) {
							textField.setText("");
						}
					}
				}
			}
		});
	}
	
	// start training 
	@FXML
	public void testerClick(ActionEvent event) {
		// input parameter
		double alphaValue = Double.parseDouble(alpha.getText());
		double gammaValue = Double.parseDouble(gamma.getText());
		int epreuveValue = Integer.parseInt(epreuve.getText());
		
		Q_Learning q_Learning = new Q_Learning(alphaValue,gammaValue,epreuveValue);
		// notification
		textArea.appendText("Commencer les tests..." + "\n");
		// training back
		new Thread(()-> {
			q_Learning.training(textArea);
			
		}).start();	
	}
	
	// append text
	public void appendText(String str) {
	       Platform.runLater(() -> textArea.appendText(str));
	}
	
	// close modal
	@FXML
	public void annulerClick(ActionEvent event) {
		((Stage) root.getScene().getWindow()).close();
	}
}
