package Controller;

import java.io.IOException;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ParamController implements Initializable{
	
	@FXML
	private AnchorPane root;
	
	@FXML
	private Button annuler;
	
	@FXML
	private Button accepte;
	
	@FXML
	private RadioButton modeIA,modeHum;
	
	@FXML
	private RadioButton facile,moyen,difficile,diy;
	
	@FXML
	private Pane paneDiff;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {}
	
	/*
	 * play with AI
	 */
	@FXML
	public void modeAI(ActionEvent event) {
		modeHum.setSelected(false);
		paneDiff.setVisible(true);
	}
	
	/*
	 * play with player
	 */
	@FXML
	public void modeHum(ActionEvent event) {
		modeIA.setSelected(false);
		paneDiff.setVisible(false);
	}
	
	// difficult 1
	@FXML
	public void facile(ActionEvent event) {
		facile.setSelected(true);
		moyen.setSelected(false);
		difficile.setSelected(false);
		diy.setSelected(false);
	}
	
	// difficult 2
	@FXML
	public void moyen(ActionEvent event) {
		moyen.setSelected(true);
		facile.setSelected(false);
		difficile.setSelected(false);
		diy.setSelected(false);
	}
	
	// difficult 3
	@FXML
	public void difficile(ActionEvent event) {
		difficile.setSelected(true);
		moyen.setSelected(false);
		facile.setSelected(false);
		diy.setSelected(false);
	}
	
	// difficult diy
	@FXML
	public void diy(ActionEvent event) {
		diy.setSelected(true);
		moyen.setSelected(false);
		facile.setSelected(false);
		difficile.setSelected(false);
	}
	
	// click cancel
	@FXML
	public void annulerClick(ActionEvent event) {
		((Stage) root.getScene().getWindow()).close();
	}
	
	// click accept send mode and level
	@FXML
	public void acceptClick(ActionEvent event) throws IOException {
		if (modeHum.isSelected()) {
			GamePageController.MODE = true;
		} else {
			GamePageController.MODE = false;
			if (facile.isSelected()) {
				GamePageController.LEVEL = 0;
			} else if (moyen.isSelected()) {
				GamePageController.LEVEL = 1;
			} else if (difficile.isSelected()) {
				GamePageController.LEVEL = 2;
			} else {
				GamePageController.LEVEL = 3;
			}
		}
		((Stage) root.getScene().getWindow()).close();
	}
}
