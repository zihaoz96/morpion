package qLearning;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javafx.scene.control.TextArea;
import service.ThreeTuple;

public class Q_Learning {
	// learning rate
	private double alpha = 0.1;

	// determines the importance of future rewards
	private double gamma = 0.9;

	// possibility random action 
	private double greedy = 0.9;
	
	// reward win
	private double rewardWin = 2.0;
	
	// reward loss
	private double rewardLoss = -1;
	
	// reward draw
	private double rewardDraw = 0.2;
	
	// last state
	private String lastState = "000000000";
	
	// last action
	private int lastAction;
	
	// before last state
	private String beforeLastState = "000000000";
	
	// player 1 strategy 
	private Map<String, Double[]> p1 = new HashMap<String, Double[]>();
	
	// player 2 strategy 
	private Map<String, Double[]> p2 = new HashMap<String, Double[]>();
	
	// epreuve
	private int epreuve = 20000; 
	
	// round 0-8
	private int round = 0;
	
	// turn player 1
	private boolean turnP1 = true;
	
	// initial 
	public Q_Learning(double alpha, double gamma, int epreuve) {
		this.alpha = alpha;
		this.gamma = gamma;
		this.epreuve = epreuve;
	}
	
	public Q_Learning() {}
	
	
	/*
	 * true : no suggestion
	 * false : there are some record good or bad
	 */
	public boolean allZero(Double[] actions) {
		for (int i = 0; i < actions.length; i++) {
			if (actions[i] != 0.0) {
				return false;
			}
		}
		return true;
	}
	
	// return list of cell empty 
	public ArrayList<Integer> checkCell(String state) {
		ArrayList<Integer> resArrayList = new ArrayList<Integer>();
		for (int i = 0; i < state.length(); i++) {
			if (state.charAt(i) == '0') {
				resArrayList.add(i);
			}
		}
		return resArrayList;
	}
	
	private int choose(Map<String, Double[]> player, String state) {
		// get all actions in this state
		Double[] state_actions = player.get(state);
		
		// get these actions empty
		ArrayList<Integer> cellFree = checkCell(state);
		
		// initial the first action we choose
		int action = 0;
		
		// random if first step or float > greedy 
		if (Math.random() > this.greedy || allZero(state_actions)) {
			action = cellFree.get(new Random().nextInt(cellFree.size()));
		} 
		
		// we choose index of the biggest in action
		else {
			double max = state_actions[cellFree.get(0)];
			action = cellFree.get(0);
			for (int i = 0; i < cellFree.size(); i++) {
				if (state_actions[cellFree.get(i)] > max) {
					max = state_actions[cellFree.get(i)];
					action = cellFree.get(i);
				}
			}
		}
		
		return action;
	}
	
	private ThreeTuple feedback(Map<String, Double[]> player, String state) {
		// get next action
		int action = choose(player, state);
		
		StringBuilder strBuilder = new StringBuilder(state);
		
		// set this action into state 
		if (turnP1) {
			strBuilder.setCharAt(action, '1');
		}else {
			strBuilder.setCharAt(action, '2');
		}
		
		// transform type string
		String nextState = strBuilder.toString();
		
		// get reward
		double reward = getReward(nextState);
		
		// if finished
		if (reward == rewardWin || reward == rewardLoss) {
			nextState = "done";
		} 
		// no finished add into HashMap
		else {
			checkState(nextState, p1);
			checkState(nextState, p2);
		}
		
		// type String double integer
		return new ThreeTuple(nextState,reward,action);
	}
	
	// calculate reward
	public double getReward(String state){
		int[][] win = {{0,1,2},{0,3,6},{0,4,8},{2,5,8},{2,4,6},{1,4,7},{3,4,5},{6,7,8}};
		for (int i = 0; i < win.length; i++) {
			if (state.charAt(win[i][0]) != '0') {
				if (state.charAt(win[i][0]) == state.charAt(win[i][1]) && state.charAt(win[i][0]) == state.charAt(win[i][2])) {
					if (state.charAt(win[i][0]) == '1') {
						// p1 win
						return rewardLoss;
					} else {
						// p2 win
						return rewardWin;
					}
				}
			}
		}
		return 0.0;
	}
	
	// get max value in this state (possibility)
	public double getMaxValue(String state, Map<String, Double[]> player) {
		Double[] actions = player.get(state);
		double max = actions[0];
		for (int i = 0; i < actions.length; i++) {
			if (actions[i] > max) {
				max = actions[i];
			}
		}		
		return max;
	}
	
	// learning algorithm
	private void learn(String state, String nextState, Double reward, int action, Map<String, Double[]> player) {
		double traget = 0;
		if (nextState == "done") {
			traget = reward;
		} else {
			traget = reward + gamma * getMaxValue(nextState, player);
		}
		
		player.get(state)[action] += alpha * (traget - player.get(state)[action]);
	}
	
	// if not exist, add
	public void checkState(String state, Map<String, Double[]> player) {
		if (!player.containsKey(state)) {
			Double[] actions = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
			player.put(state, actions);
		}
	}
	
	// training
	public void training(TextArea textArea) {
		// round condition
		int p1Win = 0;
		int p2Win = 0;
		int draw = 0;
		
		// circulate test round
		for (int i = 0; i < epreuve; i++) {
			// state initial
			String state = "000000000";
			checkState(state, p1);
			
			// fist step true
			boolean first = true;
			
			while(true) {
				int action;
				Double reward;
				String nextState;
				
				// p1 turn
				ThreeTuple fb1 = feedback(p1, state);
				
				// get state after p1 choose
				nextState = fb1.getNextState();
				
				// get reward
				reward = fb1.getReward();
				
				// get action it choose
				action = fb1.getAction();
				
				// not first step
				if (!first) {
					learn(beforeLastState, nextState, 0.0, lastAction, p2);
				}
				
				// p1 win the game
				if (reward == rewardLoss) {
					p1Win++;
					// learning result for p1
					learn(state, nextState, rewardWin, action, p1);
					
					// learning result for p2
					learn(lastState, state, rewardLoss, lastAction, p2);
					turnP1 = true;
					break;
				} 
				
				// draw game
				else if (getReward(nextState) == 0 && checkCell(nextState).size() == 0) {
					// learning result for p1
					learn(state, nextState, rewardDraw, action, p1);
					
					// learning result for p2
					learn(lastState, state, rewardDraw, lastAction, p1);
					draw ++;
					break;
				} 
				
				// set last state and action
				lastState = state;
				lastAction = action;
				
				// update state
				state = nextState;
				
				// change player
				turnP1 = false;
				
				// p2 turn
				ThreeTuple fb2 = feedback(p2, state);
				
				// get state after p2 choose
				nextState = fb2.getNextState();
				
				// get reward
				reward = fb2.getReward();
				
				// get action it choose
				action = fb2.getAction();
				
				learn(beforeLastState, nextState, 0.0, lastAction, p1);
				beforeLastState = state;
				
				// p2 win
				if (reward == rewardWin) {
					// p2 win number of time
					p2Win++;
					
					// learning result for p2
					learn(state, nextState, rewardWin, action, p2);
					
					// learning result for p1
					learn(lastState, state, rewardLoss, lastAction, p1);
					turnP1 = true;
					break;
				} 
				
				// set last state and action
				lastState = state;
				lastAction = action;
				
				// update state
				state = nextState;
				
				// change player
				turnP1 = true;
				
				// first round finished
				first = false;
			}
			// round already test
			round ++;
		}
		
		// calculate possibility
		double chanceP1 = (double) p1Win/round;
		double chanceP2 = (double) p2Win/round;
		double chanceDraw = (double) draw/round;
		
		// set into text area
		textArea.appendText("P1 win: "+ chanceP1 + " P2 win: " + chanceP2 + " Draw: " + chanceDraw + "\nLes tests sont fini!\n");
		
		// set data
		File f = new File("./levelDiy.txt");
		save(f, p2);
	}
	
	private void save(File file, Map<String, Double[]> player) {
		try {
			// write data in to file
			FileWriter out = new FileWriter(file);
			for (Map.Entry<String, Double[]> entry : player.entrySet()) {
				 
			    String state = entry.getKey();
			    Double[] actions = entry.getValue();
			    out.write(state+"\t");
			    for (int j = 0; j < actions.length; j++) {
			    	out.write(actions[j]+"\t");
				}
			    out.write("\r\n");
			}
			out.write(player.size());
			out.close();
		} catch (Exception e) {
			
		}
	}
	/*
	 * public static void main(String[] args) { Q_Learning run = new Q_Learning();
	 * run.train(); }
	 */
}
