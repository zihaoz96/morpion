package view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Launcher extends Application{
	
	public static Scene initial, gamePage;

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		Parent initialLayout = FXMLLoader.load(getClass().getResource("InitialWindow.fxml"));
		Parent gameParent = FXMLLoader.load(getClass().getResource("GamePage.fxml"));
		
		initialLayout.setStyle("-fx-background-image: url('" + "/img/background-image.png" + "')");
		
		Launcher.initial = new Scene(initialLayout, initialLayout.getLayoutY(), initialLayout.getLayoutX());
		Launcher.gamePage = new Scene(gameParent, gameParent.getLayoutY(), gameParent.getLayoutX());
		
		primaryStage.setTitle("Tic-tac-toe");
		primaryStage.setResizable(false);
		primaryStage.setWidth(1000);
		primaryStage.setHeight(650);
		primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/tic-tac-toe.png")));
		
		primaryStage.setScene(initial);
		primaryStage.show();

		
	}
	
	public static void main(String[] args)
	{
		launch(args);
	}


}
