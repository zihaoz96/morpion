package Controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class ReadData {
	
	public Map<String, Double[]> states = new HashMap<String, Double[]>();
	
	public void readFromFile(FileInputStream inputStream) {
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
				
			String str = null;
			
			// read data from text file
			while((str = bufferedReader.readLine()) != null) {
				String[] line = str.split("\t");
				Double[] actions = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
				for (int i = 1; i < line.length; i++) {
					actions[i-1] = Double.valueOf(line[i]);
				}
				states.put(line[0], actions);
			}
			//close
			inputStream.close();
			bufferedReader.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
