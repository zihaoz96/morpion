package modal;

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
 
public class ModalDialog {
	
	private Stage stage;
	
	final Stage modalStage = new Stage();
	
	private Scene modalScene;

	public ModalDialog(Scene sceneGame, Scene modalScene) {
		
		this.stage = (Stage) sceneGame.getWindow();
		this.modalScene = modalScene;
		//Initialize the Stage with type of modal
		modalStage.initModality(Modality.APPLICATION_MODAL);
		//Set the owner of the Stage 
		modalStage.initOwner(stage);
	}
	
	public ModalDialog() {
		//Initialize the Stage with type of modal
		modalStage.initModality(Modality.APPLICATION_MODAL);
		//Set the owner of the Stage 
		modalStage.initOwner(stage);
	}
	
	// modal mode
	public void modalMode(){
		modalStage.setTitle("Mode");
		
		// set coordinate
		modalStage.setX(stage.getX()+50);
		modalStage.setY(stage.getY()+50);
		
		// set icons
		modalStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/settings.png")));
		
		// zoom no
		modalStage.setResizable(false);
		
		modalStage.setScene(modalScene);
		modalStage.show();
	}
 
	// modal preference
	public void modalPreference(){
		modalStage.setTitle("Preferences");
		
		// set coordinate
		modalStage.setX(stage.getX()+50);
		modalStage.setY(stage.getY()+50);
		
		// set icons
		modalStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/settings.png")));
		
		// zoom no
		modalStage.setResizable(false);
		
		modalStage.setScene(modalScene);
		modalStage.show();
	}

	// modal help
	public void modalHelp() {
		// set title
		modalStage.setTitle("Comment fonctionne le jeu ?");
		AnchorPane anchorPane = new AnchorPane();
		anchorPane.setStyle("-fx-background-image: url('" + "/img/aide.png" + "')");
		Scene scene = new Scene(anchorPane, 1284, 802);
		
		// set icons
		modalStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/interrogation.png")));
		
		// zoom no
		modalStage.setResizable(false);
		
		modalStage.setScene(scene);
		modalStage.show();
	}
}
