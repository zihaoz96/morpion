# Morpion (tic-tac-toe)



Tic-tac-toe is a famous game. We ,the devs Mohamed and Zheng, tried to make this game using javafx and machine learning in order to train a "bot" overtime to be a perfect oppenant.



# Screenshots

![First screen](https://i.ibb.co/Mgzzc3b/1.png)
![Playing screen](https://i.ibb.co/JnZVSsW/2.png)
![You won](https://i.ibb.co/5vmRZtw/3.png)
![Settings](https://i.ibb.co/RYHbDbJ/4.png)
![Machine Learning settings](https://i.ibb.co/b2bPjkp/5.png)



You can also:
  - Choose to play vs a friend
  - Train your own AI
  - Choose the AI difficulty


### Tech

We used this techs in order to get this game working:

* [Javafx](https://openjfx.io/) - HTML enhanced for web apps!
* [Eclipse](https://www.eclipse.org/) - awesome web-based text editor

And of course gitlab : [public repository](https://gitlab.com/zihaoz96/morpion).

### Installation


Clone from git

```sh
$ git clone https://gitlab.com/zihaoz96/morpion
```

