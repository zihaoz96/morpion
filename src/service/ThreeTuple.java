package service;

public class ThreeTuple {
	private String nextState;
	private Double reward;
	private int action;
	
	public ThreeTuple(String nextState, Double reward, int action){
		this.nextState = nextState;
		this.reward = reward;
		this.action = action;
	}
	
	public String getNextState() {
		return this.nextState;
	}
	
	public Double getReward() {
		return this.reward;
	}
	
	public int getAction() {
		return this.action;
	}
}
