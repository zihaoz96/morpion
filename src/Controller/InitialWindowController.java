package Controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import view.Launcher;

public class InitialWindowController implements Initializable{

	@FXML
	private AnchorPane root;
	
	@FXML
	private ImageView imgX;

	@Override
	public void initialize(URL location, ResourceBundle resources) {}
	
	/*
	 * start to game page
	 */
	@FXML
	public void contreJoueur(ActionEvent event) {
		this.switchTo(Launcher.gamePage);
	}
	
	/*
	 * exit the game
	 */
	@FXML
	public void ExitGame(ActionEvent event) {
		// get stage
		Stage primaryStage = (Stage) this.root.getScene().getWindow();
		// close window
		primaryStage.close();
		
	}

	
	private void switchTo(Scene scene) {
		// get stage
		Stage primaryStage = (Stage) this.root.getScene().getWindow();
		
		primaryStage.setScene(scene);
		
		primaryStage.show();
	}

}
